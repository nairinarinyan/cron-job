#!/usr/bin/env node

var mongoose = require('mongoose');
var User = require('../models/User');
var Appointment = require('../models/Appointment');

var chance = new require('chance')();
var COUNT = 100;

function setUpMongoose() {
	if (!mongoose.connection.readyState) {
		mongoose.connect('mongodb://localhost/cron');
	}
	mongoose.Promise = Promise;
}

function closeConnection() {
	if (require.main === module) {
		mongoose.connection.close();
	}
}

function updateUser() {
	User.findOne()
		.then(user => {
			user = user || new User({ name: 'John', number: '12345678' });
			return user.save();
		})
		.then(user => updateAppointments(user._id));
}

function updateAppointments(userId) {
	Appointment.findOne()
		.then(apt => {
			if (!apt) {
				return createAppointments(COUNT, userId);
			}

			closeConnection();
		});
}

function createAppointments(count, userId) {
	var promises = [];
	var date = new Date();

	while(count) {
		var appointment = new Appointment({
			title: 'Appointment',
			description: 'Some appointment',
			time: chance.date({
				year: date.getFullYear(),
				month: date.getMonth(),
				day: date.getDate()
			}),
			user: userId
		});

		promises.push(appointment.save());
		count--;
	}

	Promise.all(promises)
		.then(p => closeConnection());
}

setUpMongoose();
updateUser();