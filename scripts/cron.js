var sms = require('../lib/sms');
var CronJob = require('cron').CronJob;
var Appointment = require('../models/Appointment');

// depending on whether you store the messages in some json or in the db
var messages = require('../config/messages');

// run every 30 minutes
var TIMING = '* 30 * * * *'
var HOURS_DIFF = 5;

function startSchedule() {
	var reminderJob = new CronJob({
		cronTime: TIMING,
		onTick: function() {
			checkUpcoming();
		},
		start: true
	});

	reminderJob.start();
}

function checkUpcoming() {
	Appointment
		.find({})
		.populate('user')
		.then(appointments => appointments.map(checkAppointment))
		.catch(e => console.error(e));
}

function checkAppointment(appointment) {
	var time = appointment.time;
	var date = new Date();

	if (time >= date && date.setHours(date.getHours() + HOURS_DIFF) >= time) {
		sms.sendMessage(appointment.user.number, messages.reminder);
	}
}

module.exports = startSchedule;