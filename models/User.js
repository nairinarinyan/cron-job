var mongoose = require('mongoose');

var UserSchema = new mongoose.Schema({
	name: String,
	number: String
});

module.exports = mongoose.model('User', UserSchema);