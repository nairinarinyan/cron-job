var mongoose = require('mongoose');

var AppointmentSchema = new mongoose.Schema({
	title: String,
	description: String,
	time: Date,
	user: {
		type:mongoose.Schema.Types.ObjectId,
		ref: 'User'
	}
});

module.exports = mongoose.model('Appointment', AppointmentSchema);